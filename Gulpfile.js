const gulp = require('gulp');
const sass = require('gulp-sass');

function buildCss() {
    return gulp.src('app/scss/*.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(gulp.dest('app/css'));
}

exports.default = function() {
    gulp.watch('app/scss/*.scss', buildCss);
}

exports.css = buildCss;